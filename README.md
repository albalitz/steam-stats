# Steam Stats
A CLI to view stats about your (or friends') [Steam](https://steamcommunity.com/) accounts.

## Usage
First you'll need a [Steam account](https://steampowered.com) with a valid [API key](https://steamcommunity.com/dev).  
Pass it to `steam-stats` via a `$STEAM_API_KEY` environment variable, or by putting it in `~/.config/steam-stats/api-key` as the only content.

You'll also need the Steam ID of the profile about which you want to view stats.  
You can get this by going to the profile in your web browser and copy it from the URL: `https://steamcommunity.com/profiles/<steam id>`

## Displayed Stats
Currently this displays stats about
- How many games were played and how many are owned
- Playtime in total and in the past two weeks
- Playtime by platform (Linux, Mac, Windows)  
  Note that this may be inaccurate as Steam seems to have introduced this at a certain date, so it doesn't include time played before that.
- Information about the most/least played games in total, in the past two weeks, and in total by platform
- A list of games played in the past two weeks
