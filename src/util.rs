use chrono::Duration;

/// Formats a `chrono::Duration` to a human-readable form,
/// containing hours and minutes (both only if non-zero) and appropriate singular/plural.
pub(crate) fn format_playtime(duration: Duration) -> String {
    if duration.is_zero() {
        return String::from("0 minutes");
    }
    let hours = duration.num_hours();
    let minutes = duration.num_minutes() - Duration::hours(hours).num_minutes();
    format!(
        "{}{}",
        match hours {
            0 => String::new(),
            1 => String::from("1 hour "),
            h @ _ => format!("{} hours ", h),
        },
        match minutes {
            0 => String::new(),
            1 => String::from("1 minute"),
            m @ _ => format!("{} minutes", m),
        }
    )
    .trim()
    .to_string()
}

#[cfg(test)]
mod test_format_playtime {
    use super::*;

    #[test]
    fn should_format_zero_as_0_minutes() {
        let duration = Duration::zero();
        let expected = String::from("0 minutes");
        let result = format_playtime(duration);
        assert_eq!(result, expected);
    }

    #[test]
    fn should_format_whole_hour_without_minutes_and_use_singular() {
        let duration = Duration::minutes(60);
        let expected = String::from("1 hour");
        let result = format_playtime(duration);
        assert_eq!(result, expected);
    }

    #[test]
    fn should_use_singular_for_one_minute_and_omit_hours() {
        let duration = Duration::minutes(1);
        let expected = String::from("1 minute");
        let result = format_playtime(duration);
        assert_eq!(result, expected);
    }

    #[test]
    fn should_format_under_one_hour_without_hours_and_use_plural() {
        let duration = Duration::minutes(42);
        let expected = String::from("42 minutes");
        let result = format_playtime(duration);
        assert_eq!(result, expected);
    }

    #[test]
    fn should_format_other_durations_with_hours_and_minutes_with_appropriate_pluralization() {
        let duration = Duration::minutes(90);
        let expected = String::from("1 hour 30 minutes");
        let result = format_playtime(duration);
        assert_eq!(result, expected);

        let duration = Duration::minutes(180);
        let expected = String::from("3 hours");
        let result = format_playtime(duration);
        assert_eq!(result, expected);

        let duration = Duration::minutes(125);
        let expected = String::from("2 hours 5 minutes");
        let result = format_playtime(duration);
        assert_eq!(result, expected);
    }
}
