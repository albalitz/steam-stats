use clap::ArgMatches;
use tracing::{debug, error, Level};
use tracing_subscriber::FmtSubscriber;

const API_KEY_ENV_VAR: &str = "STEAM_API_KEY";
const CONFIG_DIR_NAME: &str = "steam-stats";
const API_KEY_FILE_NAME: &str = "api-key";

#[derive(thiserror::Error, Debug)]
pub(crate) enum ConfigError {
    #[error("API key missing")]
    ApiKeyMissing,
    #[error("Steam ID missing")]
    SteamIdMissing,
}

#[derive(Debug)]
pub(crate) struct Config {
    api_key: String,
    steam_id: String,
}

impl Config {
    pub(crate) fn from_args(args: &ArgMatches) -> Result<Config, ConfigError> {
        let log_level = match args.occurrences_of("verbosity") {
            0 => Level::WARN,
            1 => Level::INFO,
            2 => Level::DEBUG,
            _ => Level::TRACE,
        };
        let subscriber = FmtSubscriber::builder().with_max_level(log_level);
        let subscriber = subscriber.finish();
        tracing::subscriber::set_global_default(subscriber).expect("configuring logging failed");

        let steam_id = match args.value_of("steam-id") {
            Some(steam_id) => String::from(steam_id),
            None => return Err(ConfigError::SteamIdMissing),
        };

        let api_key = ApiKey::new()?.as_string();

        Ok(Config { api_key, steam_id })
    }

    pub(crate) fn api_key(&self) -> &str {
        &self.api_key
    }

    pub(crate) fn steam_id(&self) -> &str {
        &self.steam_id
    }
}

struct ApiKey(String);

impl ApiKey {
    fn new() -> Result<ApiKey, ConfigError> {
        match std::env::var(API_KEY_ENV_VAR) {
            Ok(api_key) => Ok(ApiKey(api_key)),
            Err(e) => {
                debug!(
                    "Error fetching API key from environment: {} - trying config file...",
                    e
                );
                let api_key_file = match dirs::config_dir() {
                    Some(dir) => dir.join(CONFIG_DIR_NAME).join(API_KEY_FILE_NAME),
                    None => panic!("Can't find your config directory"),
                };

                match std::fs::read_to_string(&api_key_file) {
                    Ok(content) => Ok(ApiKey(content.trim().to_owned())),
                    Err(e) => {
                        error!(
                            "Error reading API key from {}: {}",
                            api_key_file.display(),
                            e
                        );
                        Err(ConfigError::ApiKeyMissing)
                    }
                }
            }
        }
    }

    fn as_string(&self) -> String {
        self.0.to_owned()
    }
}
