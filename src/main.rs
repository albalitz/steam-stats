use clap::{crate_description, crate_name, crate_version, App, Arg, ArgMatches};
use tracing::{debug, error};

mod config;
mod steam;
mod util;

use config::Config;

fn args<'a>() -> ArgMatches<'a> {
    App::new(crate_name!())
        .version(crate_version!())
        .about(crate_description!())
        .arg(Arg::with_name("steam-id").required(true).takes_value(false))
        .arg(
            Arg::with_name("verbosity")
                .short("v")
                .multiple(true)
                .help("Make output more verbose")
                .long_help("Make output more verbose. Pass multiple times to increase verbosity"),
        )
        .get_matches()
}

fn main() {
    let config = match Config::from_args(&args()) {
        Ok(config) => config,
        Err(e) => {
            eprintln!("{}", e);
            return;
        }
    };
    debug!("Running with config: {:?}", config);

    let steam_data =
        match steam::SteamData::from_api(config.api_key(), config.steam_id(), steam::api_url()) {
            Ok(sd) => sd,
            Err(e) => {
                error!("Error gathering data from Steam API: {}", e);
                return;
            }
        };
    let stats = steam::Stats::from(steam_data);
    print!("{}", stats);
}
