use std::fmt;

use chrono::Duration;
use tracing::trace;

use super::games::Game;
use super::SteamData;
use crate::util::format_playtime;

const MINIMUM_PLAYED_MINUTES: i64 = 20;
const TOP_GAMES_COUNT: usize = 10;

/// Aggregated statistics of data from `steam::games::SteamData`
pub(crate) struct Stats {
    owned_games_count: usize,
    played_games_count: usize,
    played_recently: Vec<Game>,
    top_games: Vec<Game>,
    most_played_total: Option<Game>,
    most_played_recently: Option<Game>,
    least_played_total: Option<Game>,
    least_played_recently: Option<Game>,
    most_played_total_linux: Option<Game>,
    most_played_total_mac: Option<Game>,
    most_played_total_windows: Option<Game>,
    most_used_platform: Option<Platform>,
    least_used_platform: Option<Platform>,
    total_playtime: Duration,
    total_playtime_linux: Duration,
    total_playtime_mac: Duration,
    total_playtime_windows: Duration,
    recent_playtime: Duration,
    games_played_on_linux: Vec<Game>,
    games_played_on_mac: Vec<Game>,
    games_played_on_windows: Vec<Game>,
}

impl fmt::Display for Stats {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let empty_line = String::new();
        let lines: Vec<String> = vec![
            format!(
                "Played {} of {} owned game{}",
                self.played_games_count,
                self.owned_games_count,
                match self.owned_games_count {
                    1 => "",
                    _ => "s",
                }
            ),
            format!(
                "Played {} in total; {} in the past 2 weeks",
                format_playtime(self.total_playtime),
                format_playtime(self.recent_playtime)
            ),
            empty_line.clone(),
            format!("Platforms (Note: this may not be accurate for games played before a certain date):"),
            format!(
                "  Time played on Linux: {} ({} game{})",
                format_playtime(self.total_playtime_linux),
                self.games_played_on_linux.len(),
                match &self.games_played_on_linux.len() {
                    1 => "",
                    _ => "s",
                }
            ),
            format!(
                "  Time played on Mac: {} ({} game{})",
                format_playtime(self.total_playtime_mac),
                self.games_played_on_mac.len(),
                match &self.games_played_on_mac.len() {
                    1 => "",
                    _ => "s",
                }
            ),
            format!(
                "  Time played on Windows: {} ({} game{})",
                format_playtime(self.total_playtime_windows),
                self.games_played_on_windows.len(),
                match &self.games_played_on_windows.len() {
                    1 => "",
                    _ => "s",
                }
            ),
            if self.most_used_platform.is_some() || self.least_used_platform.is_some() {
                format!(
                    "{}{}{}",
                    match &self.most_used_platform {
                        Some(platform) => format!("Played most on {};", platform),
                        None => String::new(),
                    },
                    if self.most_used_platform.is_some() && self.least_used_platform.is_some() {
                        " "
                    } else {
                        ""
                    },
                    match &self.least_used_platform {
                        Some(platform) => format!("Played least on {}", platform),
                        None => String::new(),
                    }
                )
            } else {
                format!("No specific platform seems to be favored.")
            },
            empty_line.clone(),
            match &self.most_played_total_linux {
                Some(game) => {
                    let playtime = game.playtime_linux_forever().unwrap_or(Duration::zero());
                    if !playtime.is_zero() {
                        format!(
                            "Most played game on Linux: {} ({})",
                            game.name(),
                            format_playtime(playtime)
                        )
                    } else {
                        format!("No specific game played the most on Linux.")
                    }
                },
                None => format!("No specific game played the most on Linux."),
            },
            match &self.most_played_total_mac {
                Some(game) => {
                    let playtime = game.playtime_mac_forever().unwrap_or(Duration::zero());
                    if !playtime.is_zero() {
                        format!(
                            "Most played game on Mac: {} ({})",
                            game.name(),
                            format_playtime(playtime)
                        )
                    } else {
                        format!("No specific game played the most on Mac.")
                    }
                },
                None => format!("No specific game played the most on Mac."),
            },
            match &self.most_played_total_windows {
                Some(game) => {
                    let playtime = game.playtime_windows_forever().unwrap_or(Duration::zero());
                    if !playtime.is_zero() {
                        format!(
                            "Most played game on Windows: {} ({})",
                            game.name(),
                            format_playtime(playtime)
                        )
                    } else {
                        format!("No specific game played the most on Windows.")
                    }
                }
                None => format!("No specific game played the most on Windows."),
            },
            empty_line.clone(),
            match &self.most_played_total {
                Some(game) => format!(
                    "Most played game: {} ({})",
                    game.name(),
                    format_playtime(game.playtime_forever())
                ),
                None => format!("No specific game played the most"),
            },
            match &self.least_played_total {
                Some(game) => format!(
                    "Least played game: {} ({})",
                    game.name(),
                    format_playtime(game.playtime_forever())
                ),
                None => format!("No specific game played the least"),
            },
            match &self.most_played_recently {
                Some(game) => format!(
                    "Most played game (past 2 weeks): {} ({})",
                    game.name(),
                    format_playtime(game.playtime_2weeks())
                ),
                None => format!("No specific game played the most in the past 2 weeks"),
            },
            match &self.least_played_recently {
                Some(game) => format!(
                    "Least played game (past 2 weeks): {} ({})",
                    game.name(),
                    format_playtime(game.playtime_2weeks())
                ),
                None => format!("No specific game played the least in the past 2 weeks"),
            },
            empty_line.clone(),
            if !self.top_games.is_empty() {
                let top_games: Vec<String> = self.top_games.iter().map(|game| format!("- {} ({})", game.name(), format_playtime(game.playtime_forever()))).collect();
                format!(
                    "Most played game{}:\n{}\n",
                    match top_games.len() {
                        1 => "",
                        _ => "s",
                    },
                    top_games.join("\n")
                )
            } else {
                format!("No top games.")
            },
            empty_line.clone(),
            if !self.played_recently.is_empty() {
                let recent_games: Vec<String> = self.played_recently.iter().map(|game| format!("- {} ({})", game.name(), format_playtime(game.playtime_2weeks()))).collect();
                format!("Played in the past 2 weeks:\n{}\n({} game{})", recent_games.join("\n"), recent_games.len(), match recent_games.len() {
                    1 => "",
                    _ => "s",
                })
            } else {
                format!("No games played in the past 2 weeks.")
            },
            empty_line.clone(),
        ];
        write!(f, "{}", lines.join("\n"))
    }
}

impl From<SteamData> for Stats {
    fn from(sd: SteamData) -> Stats {
        trace!("Collecting statistics about steam data ... ");

        let owned_games_count: usize = sd.game_count();
        let played_games_count: usize = Stats::collect_played_games_count(sd.games());
        let played_recently: Vec<Game> = Stats::collect_played_recently(sd.games());
        let top_games: Vec<Game> = Stats::collect_top_games(sd.games());

        let total_playtime_linux: Duration = Stats::collect_total_playtime_linux(sd.games());
        let total_playtime_mac: Duration = Stats::collect_total_playtime_mac(sd.games());
        let total_playtime_windows: Duration = Stats::collect_total_playtime_windows(sd.games());
        let total_playtime: Duration = Stats::collect_total_playtime(sd.games());
        let recent_playtime: Duration = Stats::collect_recent_playtime(sd.games());

        let platform_times: Vec<(Platform, Duration)> = vec![
            (Platform::Linux, total_playtime_linux),
            (Platform::Mac, total_playtime_mac),
            (Platform::Windows, total_playtime_windows),
        ];
        let most_used_platform: Option<Platform> =
            Stats::collect_most_used_platform(&platform_times);
        let least_used_platform: Option<Platform> =
            Stats::collect_least_used_platform(&platform_times);

        let games_played_on_linux: Vec<Game> = Stats::collect_games_played_on_linux(sd.games());
        let games_played_on_mac: Vec<Game> = Stats::collect_games_played_on_mac(sd.games());
        let games_played_on_windows: Vec<Game> = Stats::collect_games_played_on_windows(sd.games());

        let most_played_total: Option<Game> = Stats::collect_most_played_total(sd.games());
        let most_played_recently: Option<Game> = Stats::collect_most_played_recently(sd.games());
        let least_played_total: Option<Game> = Stats::collect_least_played_total(sd.games());
        let least_played_recently: Option<Game> = Stats::collect_least_played_recently(sd.games());

        let most_played_total_linux: Option<Game> =
            Stats::collect_most_played_total_linux(sd.games());
        let most_played_total_mac: Option<Game> = Stats::collect_most_played_total_mac(sd.games());
        let most_played_total_windows: Option<Game> =
            Stats::collect_most_played_total_windows(sd.games());

        trace!("Done collecting statistics");
        Stats {
            owned_games_count,
            played_games_count,
            played_recently,
            top_games,
            most_played_total,
            most_played_recently,
            least_played_total,
            least_played_recently,
            most_played_total_linux,
            most_played_total_mac,
            most_played_total_windows,
            most_used_platform,
            least_used_platform,
            total_playtime,
            total_playtime_linux,
            total_playtime_mac,
            total_playtime_windows,
            recent_playtime,
            games_played_on_linux,
            games_played_on_mac,
            games_played_on_windows,
        }
    }
}

impl Stats {
    fn collect_played_games_count(games: &Vec<Game>) -> usize {
        games
            .iter()
            .filter(|game| game.playtime_forever().num_minutes() > MINIMUM_PLAYED_MINUTES)
            .collect::<Vec<&Game>>()
            .len()
    }

    fn collect_played_recently(games: &Vec<Game>) -> Vec<Game> {
        let mut games: Vec<Game> = games
            .iter()
            .filter(|game| game.playtime_2weeks().num_minutes() > MINIMUM_PLAYED_MINUTES)
            .map(|game| game.to_owned())
            .collect();
        games.sort_by_key(|game| game.playtime_2weeks());
        games.reverse();
        games
    }

    fn collect_top_games(games: &Vec<Game>) -> Vec<Game> {
        let mut games: Vec<Game> = games.iter().map(|game| game.to_owned()).collect();
        games.sort_by_key(|game| game.playtime_forever());
        games.reverse();
        games.into_iter().take(TOP_GAMES_COUNT).collect()
    }

    fn collect_most_played_total(games: &Vec<Game>) -> Option<Game> {
        games
            .iter()
            .max_by_key(|game| game.playtime_forever())
            .map(|game| game.to_owned())
    }

    fn collect_most_played_recently(games: &Vec<Game>) -> Option<Game> {
        games
            .iter()
            .max_by_key(|game| game.playtime_2weeks())
            .map(|game| game.to_owned())
    }

    fn collect_least_played_total(games: &Vec<Game>) -> Option<Game> {
        games
            .iter()
            .filter(|game| game.playtime_forever().num_minutes() > MINIMUM_PLAYED_MINUTES)
            .min_by_key(|game| game.playtime_forever())
            .map(|game| game.to_owned())
    }

    fn collect_least_played_recently(games: &Vec<Game>) -> Option<Game> {
        games
            .iter()
            .filter(|game| game.playtime_2weeks().num_minutes() > MINIMUM_PLAYED_MINUTES)
            .min_by_key(|game| game.playtime_2weeks())
            .map(|game| game.to_owned())
    }

    fn collect_most_played_total_linux(games: &Vec<Game>) -> Option<Game> {
        games
            .iter()
            .filter(|game| game.playtime_linux_forever().is_some())
            .max_by_key(|game| game.playtime_linux_forever())
            .map(|game| game.to_owned())
    }

    fn collect_most_played_total_mac(games: &Vec<Game>) -> Option<Game> {
        games
            .iter()
            .filter(|game| game.playtime_mac_forever().is_some())
            .max_by_key(|game| game.playtime_mac_forever())
            .map(|game| game.to_owned())
    }

    fn collect_most_played_total_windows(games: &Vec<Game>) -> Option<Game> {
        games
            .iter()
            .filter(|game| game.playtime_windows_forever().is_some())
            .max_by_key(|game| game.playtime_windows_forever())
            .map(|game| game.to_owned())
    }

    fn collect_most_used_platform(platform_times: &Vec<(Platform, Duration)>) -> Option<Platform> {
        platform_times
            .iter()
            .filter(|(_p, t)| t.num_minutes() > MINIMUM_PLAYED_MINUTES)
            .max_by_key(|(_p, t)| t)
            .map(|(p, _t)| p.to_owned())
    }

    fn collect_least_used_platform(platform_times: &Vec<(Platform, Duration)>) -> Option<Platform> {
        platform_times
            .iter()
            .filter(|(_p, t)| t.num_minutes() > MINIMUM_PLAYED_MINUTES)
            .min_by_key(|(_p, t)| t)
            .map(|(p, _t)| p.to_owned())
    }

    fn collect_total_playtime(games: &Vec<Game>) -> Duration {
        Duration::minutes(
            games
                .iter()
                .map(|game| game.playtime_forever().num_minutes())
                .sum(),
        )
    }

    fn collect_total_playtime_linux(games: &Vec<Game>) -> Duration {
        Duration::minutes(
            games
                .iter()
                .map(|game| {
                    game.playtime_linux_forever()
                        .unwrap_or(Duration::zero())
                        .num_minutes()
                })
                .sum(),
        )
    }

    fn collect_total_playtime_mac(games: &Vec<Game>) -> Duration {
        Duration::minutes(
            games
                .iter()
                .map(|game| {
                    game.playtime_mac_forever()
                        .unwrap_or(Duration::zero())
                        .num_minutes()
                })
                .sum(),
        )
    }

    fn collect_total_playtime_windows(games: &Vec<Game>) -> Duration {
        Duration::minutes(
            games
                .iter()
                .map(|game| {
                    game.playtime_windows_forever()
                        .unwrap_or(Duration::zero())
                        .num_minutes()
                })
                .sum(),
        )
    }

    fn collect_recent_playtime(games: &Vec<Game>) -> Duration {
        Duration::minutes(
            games
                .iter()
                .map(|game| game.playtime_2weeks().num_minutes())
                .sum(),
        )
    }

    fn collect_games_played_on_linux(games: &Vec<Game>) -> Vec<Game> {
        games
            .iter()
            .filter(|game| {
                game.playtime_linux_forever()
                    .unwrap_or(Duration::zero())
                    .num_minutes()
                    > MINIMUM_PLAYED_MINUTES
            })
            .map(|game| game.to_owned())
            .collect()
    }

    fn collect_games_played_on_mac(games: &Vec<Game>) -> Vec<Game> {
        games
            .iter()
            .filter(|game| {
                game.playtime_mac_forever()
                    .unwrap_or(Duration::zero())
                    .num_minutes()
                    > MINIMUM_PLAYED_MINUTES
            })
            .map(|game| game.to_owned())
            .collect()
    }

    fn collect_games_played_on_windows(games: &Vec<Game>) -> Vec<Game> {
        games
            .iter()
            .filter(|game| {
                game.playtime_windows_forever()
                    .unwrap_or(Duration::zero())
                    .num_minutes()
                    > MINIMUM_PLAYED_MINUTES
            })
            .map(|game| game.to_owned())
            .collect()
    }
}

#[derive(Debug, PartialEq, Clone)]
enum Platform {
    Linux,
    Mac,
    Windows,
}

impl fmt::Display for Platform {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Platform::Linux => "Linux",
                Platform::Mac => "Mac",
                Platform::Windows => "Windows",
            }
        )
    }
}

#[cfg(test)]
mod test_stats_from_steamdata {
    use super::*;

    #[test]
    fn should_collect_statistics_as_expected() {
        let steam_data = SteamData::new(vec![
            Game::new(
                1337,
                "Rust Test",
                4242,
                2121,
                Some(2121),
                None,
                Some(2121),
                true,
            ),
            Game::new(
                42,
                "Another Test",
                133337,
                1337,
                Some(124337),
                Some(1337),
                Some(7663),
                false,
            ),
            Game::new(43, "Not Played", 0, 0, None, None, None, false),
        ]);
        let stats = Stats::from(steam_data);

        assert_eq!(stats.owned_games_count, 3, "Should count owned games");
        assert_eq!(
            stats.played_games_count, 2,
            "Should count games played at least 1 minute"
        );
        assert_eq!(
            stats.total_playtime,
            Duration::minutes(137579),
            "Total playtime should be the sum of all playtimes"
        );
        assert_eq!(
            stats.total_playtime_linux,
            Duration::minutes(126458),
            "Total playtime on linux should be the sum of all playtimes on linux"
        );
        assert_eq!(
            stats.total_playtime_mac,
            Duration::minutes(1337),
            "Total playtime on mac should be the sum of all playtimes on mac"
        );
        assert_eq!(
            stats.total_playtime_windows,
            Duration::minutes(9784),
            "Total playtime on windows should be the sum of all playtimes on windows"
        );
        assert_eq!(
            stats.recent_playtime,
            Duration::minutes(3458),
            "Recent playtime should be the sum of all playtimes in the past 2 weeks"
        );
        assert_eq!(
            stats.most_played_total.unwrap().appid(),
            42,
            "Should find most played game of all time"
        );
        assert_eq!(
            stats.most_played_recently.unwrap().appid(),
            1337,
            "Should find most played game in last two weeks"
        );
        assert_eq!(
            stats.least_played_total.unwrap().appid(),
            1337,
            "Should find least played game of all time with at least 1 minute played"
        );
        assert_eq!(
            stats.least_played_recently.unwrap().appid(),
            42,
            "Should find least played game in last two weeks"
        );
        assert_eq!(
            stats.most_played_total_linux.unwrap().appid(),
            42,
            "Should find most played game on linux"
        );
        assert_eq!(
            stats.most_played_total_mac.unwrap().appid(),
            42,
            "Should find most played game on mac"
        );
        assert_eq!(
            stats.most_played_total_windows.unwrap().appid(),
            42,
            "Should find most played game on windows"
        );
        assert_eq!(
            stats.most_used_platform,
            Some(Platform::Linux),
            "Should find most used platform"
        );
        assert_eq!(
            stats.least_used_platform,
            Some(Platform::Mac),
            "Should find least used platform"
        );
        assert_eq!(
            stats.games_played_on_linux.len(),
            2,
            "Should find games played on linux"
        );
        assert_eq!(
            stats.games_played_on_mac.len(),
            1,
            "Should find games played on mac"
        );
        assert_eq!(
            stats.games_played_on_windows.len(),
            2,
            "Should find games played on windows"
        );
    }

    #[test]
    fn should_set_fields_appropriately_for_no_available_game() {
        let steam_data = SteamData::new(vec![]);
        let stats = Stats::from(steam_data);

        assert_eq!(stats.owned_games_count, 0, "Owned games should be 0");
        assert_eq!(stats.played_games_count, 0, "Played games should be 0");
        assert_eq!(
            stats.total_playtime,
            Duration::zero(),
            "Total playtime should be 0"
        );
        assert_eq!(
            stats.total_playtime_linux,
            Duration::zero(),
            "Total playtime on linux should be 0"
        );
        assert_eq!(
            stats.total_playtime_mac,
            Duration::zero(),
            "Total playtime on mac should be 0"
        );
        assert_eq!(
            stats.total_playtime_windows,
            Duration::zero(),
            "Total playtime on windows should be 0"
        );
        assert_eq!(
            stats.recent_playtime,
            Duration::zero(),
            "Recent playtime should be 0"
        );
        assert!(
            stats.most_played_total.is_none(),
            "There should be no game with the most playtime in total"
        );
        assert!(
            stats.most_played_recently.is_none(),
            "There should be no game with the most playtime in the last 2 weeks"
        );
        assert!(
            stats.least_played_total.is_none(),
            "There should be no game with the most playtime in total"
        );
        assert!(
            stats.least_played_recently.is_none(),
            "There should be no game with the most playtime in the last 2 weeks"
        );
        assert!(
            stats.most_played_total_linux.is_none(),
            "There should be no game played on linux"
        );
        assert!(
            stats.most_played_total_mac.is_none(),
            "There should be no game played on mac"
        );
        assert!(
            stats.most_played_total_windows.is_none(),
            "There should be no game played on windows"
        );
        assert!(
            stats.most_used_platform.is_none(),
            "There should be no most used platform"
        );
        assert!(
            stats.least_used_platform.is_none(),
            "There should be no least used platform"
        );
        assert!(
            stats.games_played_on_linux.is_empty(),
            "There should be no game played on linux"
        );
        assert!(
            stats.games_played_on_mac.is_empty(),
            "There should be no game played on mac"
        );
        assert!(
            stats.games_played_on_windows.is_empty(),
            "There should be no game played on windows"
        );
    }
}
