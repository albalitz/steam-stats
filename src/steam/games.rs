use chrono::Duration;
use serde::Deserialize;
use tracing::{debug, trace};
use url::Url;

use super::api::{ApiError, ApiResponse};

/// Playtime data from the Steam API.
///
/// See https://developer.valvesoftware.com/wiki/Steam_Web_API#GetOwnedGames_.28v0001.29 for
/// documentation about the fields.
#[derive(Debug, Deserialize)]
pub(crate) struct SteamData {
    game_count: usize,
    games: Vec<Game>,
}

impl SteamData {
    pub(crate) fn from_api(
        api_key: &str,
        steam_id: &str,
        api_url: Url,
    ) -> Result<SteamData, ApiError> {
        trace!("Requesting SteamData from Steam API ... ");
        let steam_data = ApiResponse::from_api(api_key, steam_id, api_url)?.response();
        debug!(
            "Got data with {} game{} from Steam",
            steam_data.game_count,
            match steam_data.game_count {
                1 => "",
                _ => "s",
            }
        );
        Ok(steam_data)
    }

    #[cfg(test)]
    pub(crate) fn new(games: Vec<Game>) -> SteamData {
        let game_count = games.len();
        SteamData { game_count, games }
    }

    pub(crate) fn game_count(&self) -> usize {
        self.game_count
    }

    pub(crate) fn games(&self) -> &Vec<Game> {
        &self.games
    }
}

/// A Game as returned as part of the Steam API's response.
///
/// See https://developer.valvesoftware.com/wiki/Steam_Web_API#GetOwnedGames_.28v0001.29 for
/// documentation about the fields.
#[derive(Debug, Deserialize, Clone)]
pub(crate) struct Game {
    appid: usize,
    name: String,
    playtime_forever: i64,
    #[serde(default)]
    playtime_2weeks: i64,
    playtime_linux_forever: Option<i64>,
    playtime_mac_forever: Option<i64>,
    playtime_windows_forever: Option<i64>,
    #[serde(default)]
    has_community_visible_stats: bool,
}

impl Game {
    #[cfg(test)]
    pub(super) fn new(
        appid: usize,
        name: &str,
        playtime_forever: i64,
        playtime_2weeks: i64,
        playtime_linux_forever: Option<i64>,
        playtime_mac_forever: Option<i64>,
        playtime_windows_forever: Option<i64>,
        has_community_visible_stats: bool,
    ) -> Game {
        let name = name.to_string();
        Game {
            appid,
            name,
            playtime_forever,
            playtime_2weeks,
            playtime_linux_forever,
            playtime_mac_forever,
            playtime_windows_forever,
            has_community_visible_stats,
        }
    }

    pub(crate) fn appid(&self) -> usize {
        self.appid
    }

    pub(crate) fn name(&self) -> &str {
        &self.name
    }

    pub(crate) fn playtime_forever(&self) -> Duration {
        Duration::minutes(self.playtime_forever)
    }

    pub(crate) fn playtime_2weeks(&self) -> Duration {
        Duration::minutes(self.playtime_2weeks)
    }

    pub(crate) fn playtime_windows_forever(&self) -> Option<Duration> {
        self.playtime_windows_forever
            .map(|minutes| Duration::minutes(minutes))
    }

    pub(crate) fn playtime_mac_forever(&self) -> Option<Duration> {
        self.playtime_mac_forever
            .map(|minutes| Duration::minutes(minutes))
    }

    pub(crate) fn playtime_linux_forever(&self) -> Option<Duration> {
        self.playtime_linux_forever
            .map(|minutes| Duration::minutes(minutes))
    }

    pub(crate) fn has_community_visible_stats(&self) -> bool {
        self.has_community_visible_stats
    }
}
