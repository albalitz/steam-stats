//! This module handles the interaction with the Steam API.
use std::time::Duration;

use serde::Deserialize;
use tracing::trace;
use url::Url;

use super::SteamData;

const API_DOMAIN: &str = "https://api.steampowered.com";
const API_GAME_STATS_PATH: &str = "/IPlayerService/GetOwnedGames/v0001";

const API_KEY_PARAM: &str = "key";
const STEAM_ID_PARAM: &str = "steamid";
const FORMAT_PARAM: &str = "format";
const REQUEST_FORMAT: &str = "json";
const APPINFO_PARAM: &str = "include_appinfo";
const APPINFO_VALUE: &str = "true";

/// Generates the URL used to request data from the Steam API.
///
/// This exists so we can have the domain and the location separate
/// and only modify the domain when running tests.
pub(crate) fn api_url() -> Url {
    Url::parse(API_DOMAIN)
        .expect("expected valid API domain")
        .join(API_GAME_STATS_PATH)
        .expect("expected valid URL")
}

#[derive(thiserror::Error, Debug)]
pub(crate) enum ApiError {
    #[error("Error gathering data from Steam API: {0}")]
    Request(#[from] reqwest::Error),
}

/// Wrapper struct for API interaction.
///
/// The actual data we're interested in is held in `steam::games::SteamData`.
#[derive(Debug, Deserialize)]
pub(super) struct ApiResponse {
    response: SteamData,
}

impl ApiResponse {
    /// Queries the API for information.
    ///
    /// This handles authentication and what information exactly we want.
    ///
    /// See https://developer.valvesoftware.com/wiki/Steam_Web_API#GetOwnedGames_.28v0001.29 for
    /// information about the query arguments.
    pub(super) fn from_api(
        api_key: &str,
        steam_id: &str,
        api_url: Url,
    ) -> Result<ApiResponse, ApiError> {
        let client = reqwest::blocking::Client::builder()
            .timeout(Duration::from_secs(10))
            .build()?;
        trace!("Requesting ApiResponse from Steam API ... ");
        let response = client
            .get(api_url)
            .query(&[
                (API_KEY_PARAM, api_key),
                (STEAM_ID_PARAM, steam_id),
                (FORMAT_PARAM, REQUEST_FORMAT),
                (APPINFO_PARAM, APPINFO_VALUE),
            ])
            .send()?
            .json::<ApiResponse>()?;
        trace!("Got response from API: {:#?}", response);
        Ok(response)
    }

    pub(super) fn response(self) -> SteamData {
        self.response
    }
}

#[cfg(test)]
mod test_steamdata_from_api {
    use chrono::Duration;
    use httpmock::Method::GET;
    use httpmock::MockServer;

    use super::*;

    #[test]
    fn should_include_game_name_and_playtimes() {
        let server = MockServer::start();
        let response_json = r#"{
  "response": {
    "game_count": 1,
    "games": [
      {
        "appid": 133337,
        "name": "Rust Test",
        "playtime_2weeks": 4242,
        "playtime_forever": 4242,
        "has_community_visible_stats": true,
        "playtime_windows_forever": 0,
        "playtime_linux_forever": 4242
      }
    ]
  }
}"#;
        let games_mock = server.mock(|when, then| {
            when.method(GET)
                .path(API_GAME_STATS_PATH)
                .query_param("key", "s3cr3t")
                .query_param("steamid", "133333333333337")
                .query_param("format", "json")
                .query_param("include_appinfo", "true");
            then.status(200)
                .header("Content-Type", "application/json")
                .body(response_json);
        });

        let steam_data = SteamData::from_api(
            "s3cr3t",
            "133333333333337",
            Url::parse(&server.url(API_GAME_STATS_PATH)).unwrap(),
        )
        .unwrap();

        games_mock.assert();

        assert_eq!(steam_data.game_count(), 1);
        let game = &steam_data.games()[0];
        assert_eq!(game.appid(), 133337, "Should have expected app ID");
        assert_eq!(
            game.name(),
            String::from("Rust Test"),
            "Should have expected name"
        );
        assert_eq!(
            game.playtime_2weeks(),
            Duration::minutes(4242),
            "Should have expected play time for the last 2 weeks"
        );
        assert_eq!(
            game.playtime_forever(),
            Duration::minutes(4242),
            "Should have expected total play time"
        );
        assert_eq!(
            game.playtime_windows_forever(),
            Some(Duration::zero()),
            "Should have expected optional platform-specific playtime, if included in response, even if 0"
        );
        assert_eq!(
            game.playtime_linux_forever(),
            Some(Duration::minutes(4242)),
            "Should have expected optional platform-specific playtime, if included in response"
        );
        assert_eq!(
            game.playtime_mac_forever(),
            None,
            "Should leave out optional platform-specific playtime, if not included in response"
        );
        assert!(
            game.has_community_visible_stats(),
            "Should know about visible stats"
        );
    }
}
